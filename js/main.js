$( document ).ready(function() {
    $('.ui.form')
        .form({
          fields: {
            name: {
                identifier  : 'name',
                rules: [
                  {
                    type   : 'minLength[3]',
                    prompt : 'Minimum 3 Karakter'
                  },
                  {
                    type   : 'empty',
                    prompt : 'Please enter your name'
                  }
                ]
            },
            email: {
              identifier  : 'email',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your e-mail'
                },
                {
                  type   : 'email',
                  prompt : 'Please enter a valid e-mail'
                }
              ]
            },
            phone: {
              identifier  : 'phone',
              rules: [
                {
                  type   : 'number',
                  prompt : 'Hanya Boleh Angka'
                }
              ]
            },
            alamat: {
                identifier  : 'alamat',
                rules: [
                  {
                    type   : 'maxLength[100]',
                    prompt : 'Maksimum 100 Karakter'
                  }
                ]
            },
            username: {
                identifier  : 'username',
                rules: [
                {
                    type   : 'empty',
                    prompt : 'Please enter your e-mail'
                  }
                ]
            },
            password: {
                identifier  : 'password',
                rules: [
                    {
                        type   : 'minLength[8]',
                        prompt : 'Minimum 3 Karakter'
                      },
                      {
                        type   : 'maxLength[16]',
                        prompt : 'Maksimal 16 Karakter'
                      }
                ]
            },
            repassword: {
                identifier  : 'repassword',
                rules: [
                    {
                        type   : 'match[password]',
                        prompt : 'Password harus sama'
                      }
                ]
            },
            agree: {
                identifier  : 'agree',
                rules: [
                    {
                        type   : 'checked',
                        prompt : 'You must agree to the terms and conditions'
                      }
                ]
            }
          }
        })
      ;
});